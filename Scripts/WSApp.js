﻿var WSApp = function () {
    var self = this;
    self.ws = {};

    self.init = function () {
        var port = window.location.port;
        if ('WebSocket' in window) {
            self.ws = new WebSocket("ws://" + window.location.hostname + ":" + port + "/api/WebSocket");
        } else if ('MozWebSocket' in window) {
            self.ws = new MozWebSocket("ws://" + window.location.hostname + ":" + port + "/api/WebSocket");
        } else return;

        self.setupSocketEvents();
    }

    self.onopen = function () {
        console.log('open connection');
    }

    self.onmessage = function (e)
    {
        console.log('Received: ' + e.data);
        $('#receivedData').append('<span>' + e.data + '</span></br>');
    }
    self.onerror = function (e)
    {
        console.log(e);
    }
    self.onclose = function (e) {
        console.log(e);
    }
    
    self.send = function (msg)
    {
        if (self.ws.readyState == WebSocket.OPEN) {
            self.ws.send(msg);
            console.info('Sending: ' + msg);
        }
    }
    self.close = function (e) {
        console.log('socket closed');
    }
    self.setupSocketEvents = function ()
    {
        self.ws.onopen = function (e) { self.onopen(e); };
        self.ws.onmessage = function (e) { self.onmessage(e); };
        self.ws.onerror = function (e) { self.onerror(e); };
        self.ws.onclose = function (e) { self.onclose(e); };
    }
}