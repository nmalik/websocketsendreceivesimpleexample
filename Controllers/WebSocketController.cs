﻿using Microsoft.Web.WebSockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace websocketTest.Controllers
{
    public class WebSocketController : ApiController
    {
        private static WebSocketCollection connections = new WebSocketCollection();
        public HttpResponseMessage Get()
        {
            if (HttpContext.Current.IsWebSocketRequest)
            {
                var mySocketHandler = new MySocketHandler();
                HttpContext.Current.AcceptWebSocketRequest(mySocketHandler);
            }

            return new HttpResponseMessage(HttpStatusCode.SwitchingProtocols);
        }

        internal class MySocketHandler : WebSocketHandler
        {
            public MySocketHandler()
            {

            }

            public override void OnClose()
            {
                connections.Remove(this);
            }

            public override void OnOpen()
            {
                connections.Add(this);
            }

            public override void OnError()
            {
                connections.Remove(this);
            }

            public override void OnMessage(byte[] message)
            {
                base.OnMessage(message);
            }

            public override void OnMessage(string message)
            {
                foreach (var conn in connections)
                {
                    conn.Send(message);
                }
            }
        }

    }
}
