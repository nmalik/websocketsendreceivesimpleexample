﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace websocketTest.Models
{
    public class MessageData
    {
        public string text { get; set; }
        public byte[] image { get; set; }
    }
}